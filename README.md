met-graph
=========

## Overview

This project is an interactive graph visualization using the Metropolitan Museum of Art's [Open Access database](https://github.com/metmuseum/openaccess) and [Art Collection API](https://metmuseum.github.io/), showcasing their modern art collection. Read more about Met Open Access [here](https://www.metmuseum.org/about-the-met/policies-and-documents/open-access).

A live version of this site can be viewed here: [met-graph-modern.red-deleon.com](http://met-graph-modern.red-deleon.com/)

## Sips image processing reference

    ```
    for i in *.jpg; do sips -s format jpeg -s formatOptions 80 "${i}" --out "./done/${i%.jpg}.jpg"; done
    ```
