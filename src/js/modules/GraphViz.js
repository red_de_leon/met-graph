/**
 *  GraphViz.js
 *
 *
 */

import ForceGraph3D from '../lib/3d-force-graph/3d-force-graph';
import SpriteText from '../lib/three-spritetext/three-spritetext';
import axios from 'axios';
import _ from 'lodash';
import { ColorPalette } from './ColorPalette';

const INIT = {
        fontSizes: {
          artwork: 200,
          year:    500
        },
        force: {
          strength: -10000
        },
        graphID: 'graph',
        edgeLength: {
          artwork: 1000,
          year:    1000,
          accession: 1000
        },
        zoom: 10000,
        zoomTarget: 20000,
        zoomIncrement: 70,
        zoomDelay: 2000
      },
      $graph = document.getElementById(INIT.graphID),
      linkColors = {
        accession:      ColorPalette.red,
        artwork:        ColorPalette.red,
        year:           ColorPalette.white,
      }, 
      textColors = {
        accession:      ColorPalette.red,
        artwork:        ColorPalette.red,
        year:           ColorPalette.white,
      };

let $detail = {},
    $panelCloseButton = null,
    $panelRight = null,
    allowRotation = true,
    dataTimestamp = '',
    graph = null,
    graphData = {
      nodes: [],
      links: []
    },
    introZoom = true,
    labelReferences = [],
    nodeCount = 0,
    sourceJSON = '',
    sourceJSONSubset = '',
    spinning = true,
    timeStart = null;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function GraphViz() {
  // Pre-prep
  const docBody = document.getElementById('body');
  sourceJSON = docBody.dataset.sourceJson;
  sourceJSONSubset = docBody.dataset.sourceJsonSubset;
  dataTimestamp = docBody.dataset.dataTimestamp;
  // Prep
  prepareTemplates();
  fetchData();
  timeStart = Date.now();
}

function onWindowResize() {
  graph
    .width(window.innerWidth * 1)
    .height(window.innerHeight);
}

function getDOMByClass(className) {
  return document.getElementsByClassName(className)[0];
}

function buildGraph(graphData) {
  graph = ForceGraph3D()
    (document.getElementById(INIT.graphID))
      .backgroundColor('rgba(0,0,0,0)')
      .dagMode(null)
      /*.dagNodeFilter(false)*/
      .graphData(graphData)
      .nodeOpacity(.8)
      .nodeVal(1000)
      .nodeResolution(12)
      .nodeColor(() => ColorPalette.gold)
      .linkCurvature('curvature')
      .linkCurveRotation('rotation')
      .linkOpacity(0.75)
      .linkColor((l) => {
        return assignColor(linkColors, l).col;
      })
      .nodeThreeObject(node => {
        // if (node.type === 'artwork') {
        //   return null;
        // } else {
          let { col } = assignColor(textColors, node),
              sprite,
              label = (node.type === 'artwork') ? node.medium : node.label;
          sprite = labelReferences[node.id] = new SpriteText(label);
          sprite.fontFace = 'Abel';
          sprite.padding = 5;
          sprite.material.depthWrite = false;
          sprite.color = col;
          sprite.colorOriginal = col;
          sprite.textHeight = INIT.fontSizes[node.type];
          sprite.textHeightOriginal = INIT.fontSizes[node.type];
          sprite.name = 'spriteTextOff';
          return sprite;
        // }
      })
      .onNodeHover((node, prevNode) => {
        if ($graph) {
          $graph.style.cursor = (node && (node.type === 'artwork')) ? 'pointer' : null;
        }
        if ((node !== null) &&
            (node.__threeObj) &&
            (node.__threeObj !== null)) {
          node.__threeObj.color = ColorPalette.white;
        }
        if ((prevNode !== null) &&
            (prevNode.__threeObj) &&
            (prevNode.__threeObj !== null)) {
          prevNode.__threeObj.color = prevNode.__threeObj.colorOriginal;
        }
      })
      .onNodeClick((node) => {
        if (node && 
            node.type === 'year' && 
            node.type === 'accession') return;
        viewNodeDetails(node);
      });

  graph.d3Force('charge').strength(INIT.force.strength);
  graph.d3Force('link').distance((n) => {
    return INIT.edgeLength[n.type];
  });
  graph.cameraPosition({ z: INIT.zoom });
  graph.scene().rotation.y = Math.random() * 360;
  // graph.onEngineTick(inspectGraph);
  spinGraph();
  window.addEventListener('resize', onWindowResize);
  onWindowResize();
}

function prepareTemplates() {
  let wrapper = document.getElementById('detail-template-wrapper'),
      templateHTML = document.getElementById('detail-template').innerHTML;
  wrapper.innerHTML = templateHTML;
  $panelRight = getDOMByClass('panel--right');
  $detail = {
    $image: getDOMByClass('detail__item--image').getElementsByTagName('img')[0],
    $title: getDOMByClass('detail__title').getElementsByTagName('a')[0],
    $year: getDOMByClass('detail__item--year').getElementsByTagName('dd')[0],
    $artist: getDOMByClass('detail__item--artist').getElementsByTagName('dd')[0],
    $medium: getDOMByClass('detail__item--medium').getElementsByTagName('dd')[0],
    $accession: getDOMByClass('detail__item--accession').getElementsByTagName('dd')[0],
  };
  $panelCloseButton = getDOMByClass('button--close');
  $panelCloseButton.addEventListener('click', onPanelClose);
}

function viewNodeDetails(node) {
  let imagePath;
  if (!node) {
    $panelRight.classList.add("hidden");
    allowRotation = true;
  } else if (node.type === 'artwork') {
    allowRotation = false;
    // Clear
    $detail.$image.setAttribute('src', '');
    $detail.$image.setAttribute('alt', '');
    $detail.$title.innerHTML = '';
    $detail.$year.innerHTML = '';
    $detail.$artist.innerHTML = '';
    $detail.$title.setAttribute('href', '');
    $detail.$medium.innerHTML = '';
    $detail.$accession.innerHTML = '';
    // Update
    imagePath = './img/modern/' + node.object_id + '.jpg';
    $panelRight.classList.remove("hidden");
    $detail.$image.setAttribute('alt', node.label);
    $detail.$image.setAttribute('src', imagePath);
    $detail.$title.innerHTML = node.label;
    $detail.$title.setAttribute('href', (node.link_resource) ? node.link_resource : '');
    $detail.$year.innerHTML = node.year;
    $detail.$artist.innerHTML = node.artist;
    $detail.$medium.innerHTML = node.medium;
    $detail.$accession.innerHTML = node.accession;
  } else {
    $panelRight.classList.add("hidden");
    allowRotation = true;
  }
}

function onPanelClose() {
  viewNodeDetails(null);
}

function assignColor(type, node) {
  let col = type[node.type];
  return { col };
}

// function inspectGraph() {
// }

function spinGraph() {
  if (allowRotation === true) {
    graph.scene().rotation.y += 0.0005;
  }
  if ((Date.now() - timeStart) > INIT.zoomDelay) {
    if ((introZoom === true) &&
        (graph.cameraPosition().z < INIT.zoomTarget)) {
      let newZ = graph.cameraPosition().z += INIT.zoomIncrement;
      graph.cameraPosition({ z: newZ });
    } else {
      introZoom = false;
    }
  }
  if (spinning) {
    requestAnimationFrame(spinGraph);
  }
}

function isIOS() {
  return [
    'iPad Simulator',
    'iPhone Simulator',
    'iPod Simulator',
    'iPad',
    'iPhone',
    'iPod'
  ].includes(navigator.platform)
  // iPad on iOS 13 detection
  || (navigator.userAgent.includes("Mac") && "ontouchend" in document)
}

function fetchData() {
  const sourceData = (isIOS() === true) ? sourceJSONSubset : sourceJSON;
  axios
    .get('./data/' + sourceData + '?' + dataTimestamp)
    .then(data => {
      // nodeCount = data.data.length;
      // console.log('nodeCount: ' + nodeCount);
      setTimeout(() => { processData(data) }, 200);
    })
    .catch(error => {
      console.log(error);
    });
}

function getYearNodes(source) {
  let temp = [],
      uniqueYears,
      uniqueYearsReformat,
      combinedList = [];

  _.each(source, (y) => { combinedList.push(y.year); });
  // _.each(source, (y) => { combinedList.push(y.accession); });
  uniqueYears = combinedList.filter(function(item) {
    if (temp[item]) return false;
    temp[item] = true;
    return true;
  });
  uniqueYears = uniqueYears.sort((a, b) => {
    return (a > b) ? 1 : -1; 
  });
  uniqueYearsReformat = uniqueYears.map((item) => {
    return { 
      id: 'y' + item,
      label: item,
      type: 'year'
    };
  });

  return [...uniqueYearsReformat];
}

function getLinks(source1, combinedList) {
  let links = [],
      artworks = [...source1],
      years = [...combinedList];
  _.each(years, (y, i) => {
    if (i < (years.length - 1)) {
      links.push({
        source: y.id,
        target: years[i + 1].id,
        type: 'year',
        value: 10,
        curvature: 0,
        rotation: 0,
        dist: 0
      });
    }
  });
  for (let i = 0; i < artworks.length; i++) {
    for (let j = 0; j < years.length; j++) {
      if (years[j].label === artworks[i].year) {
        links.push({
          source: years[j].id,
          target: artworks[i].id,
          type: 'artwork',
          value: 10,
          curvature: 0,
          rotation: 0,
          dist: 0
        });
      }
    }
  }
  // for (let i = 0; i < artworks.length; i++) {
  //   for (let j = 0; j < years.length; j++) {
  //     if (years[j].label === artworks[i].accession) {
  //       links.push({
  //         source: artworks[i].id,
  //         target: years[j].id,
  //         type: 'accession',
  //         value: 10,
  //         curvature: .7,
  //         rotation: 0,
  //         dist: (years[j].label - artworks[i].accession) * 1000
  //       });
  //     }
  //   }
  // }
  return [...links];
}

function processData(source) {
  let convertedItems = [],
      originalItems = [...source.data],
      yearNodes;

  convertedItems = originalItems.map((item, i) => ({
    id: i,
    year: parseInt(item.object_end_date, 10),
    label: item.title,
    type: 'artwork',
    link_resource: item.link_resource,
    object_id: item.object_id,
    artist: (item.artist_display_name).replace(/\|/g," | "),
    medium: item.medium,
    accession: parseInt(item.accession_year, 10)
  }));
  convertedItems = convertedItems.sort(
    (a, b) => (a.year > b.year) ? 1 : -1
  );
  graphData.nodes = [...convertedItems];
  yearNodes = getYearNodes(convertedItems);
  graphData.nodes = [...graphData.nodes, ...yearNodes];
  graphData.links = getLinks(graphData.nodes, yearNodes);
  buildGraph(graphData);
  nodeCount = convertedItems.length;
  console.log('nodeCount: ' + nodeCount);
}

export default GraphViz;
