/**
 *   ColorPalette.js
 *
 *
 */

export const ColorPalette = {
  red:        '#e4002b',
  dkRed:      '#320009',
  white:      '#ffffff',
  black:      '#000000',
  gold:       '#fbc740'
};
