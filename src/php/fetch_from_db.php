<?php
  header('Content-Type: application/json');
  $conn = new mysqli("localhost", "met_objects", "www7tWV!AzQ.hc", "met_objects");
  if ($conn->connect_error) {
    die("ERROR: Unable to connect: " . $conn->connect_error);
  } 
  /*$queryString = "SELECT object_id, title, classification, culture, artist_display_name, object_date, object_begin_date, object_end_date, medium, country, link_resource FROM objects where department like 'musical instruments'";*/
  $queryString = "SELECT object_id, title, classification, culture, artist_display_name, object_date, object_begin_date, object_end_date, medium, country, link_resource FROM objects where department like '%european paintings%'";
  $result = $conn->query($queryString);
  /*echo "Number of rows: $result->num_rows";*/
  $rows = array();
  while($r = mysqli_fetch_assoc($result)) {
    $rows[] = $r;
  }
  print json_encode($rows);
  $result->close();
  $conn->close();
?>
