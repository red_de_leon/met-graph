<?php
  header('Content-Type: application/json');
  require_once("./lib/dotenv.php");
  use DevCoder\DotEnv;
  (new DotEnv("../../.env"))->load();
  $conn = new mysqli("localhost", getenv('DATABASE_USERNAME'), getenv('DATABASE_PASSWORD'), getenv('DATABASE_NAME'));
  if ($conn->connect_error) {
    die("ERROR: Unable to connect: " . $conn->connect_error);
  } 
  /*echo 'Connected to the database.<br>';*/
  $queryString = "SELECT";
  $queryString .= " ";

  $queryString .= "object_id";
  $queryString .= ",";
  $queryString .= "title";
  $queryString .= ",";
  $queryString .= "accession_year";
  $queryString .= ",";
  $queryString .= "object_name";
  $queryString .= ",";
  $queryString .= "artist_display_name";
  $queryString .= ",";
  $queryString .= "artist_alpha_sort";
  $queryString .= ",";
  $queryString .= "artist_begin_date";
  $queryString .= ",";
  $queryString .= "artist_end_date";
  $queryString .= ",";
  $queryString .= "artist_ulan_url";
  $queryString .= ",";
  $queryString .= "artist_wikidata_url";
  $queryString .= ",";
  $queryString .= "object_date";
  $queryString .= ",";
  $queryString .= "object_begin_date";
  $queryString .= ",";
  $queryString .= "object_end_date";
  $queryString .= ",";
  $queryString .= "medium";
  $queryString .= ",";
  $queryString .= "link_resource";
  $queryString .= ",";
  $queryString .= "object_wikidata_url";
  $queryString .= ",";
  $queryString .= "tags";

  $queryString .= " FROM objects";
  $queryString .= " where department like '%Modern%'";
  $queryString .= " AND is_public_domain LIKE '%True%'";

  $result = $conn->query($queryString);
  /*echo "Number of rows: $result->num_rows";*/
  $rows = array();
  while($r = mysqli_fetch_assoc($result)) {
    $rows[] = $r;
  }
  print json_encode($rows);
  $result->close();
  $conn->close();
?>
